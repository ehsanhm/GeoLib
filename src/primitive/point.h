//
//  point.h
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-19.
//

#ifndef point_h
#define point_h

#include "vector.h"

namespace geo
{

typedef Vector2f Point2D;
typedef Vector3f Point3D;

}

#endif /* point_h */
