//
//  line.h
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-22.
//

#ifndef line_h
#define line_h

#include "vector.h"

namespace geo {

class Line
{
    Point3D point;
    Vector3f dir;
    
public:
    Line(const Point3D& p1, const Point3D& p2)
    {
        dir = p2 - p1;
        point = p1;
        std::cout << "Line 2 points constructor called!" << std::endl;
    }
    
    Point3D getPoint() const
    {
        return point;
    }
    
    Vector3f getDir() const
    {
        return dir;
    }
};

}

#endif /* line_h */
