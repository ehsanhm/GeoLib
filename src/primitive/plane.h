//
//  plane.h
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-23.
//

#ifndef plane_h
#define plane_h

#include "vector.h"

namespace geo
{

class Plane
{
    Vector3f normal;
    float d; // constant part of plane equation, normal.dot(q)
    
public:
    Plane(const Vector3f& _normal, const float& _constant): normal(_normal), d(_constant){}
    Plane(const Point3D& _p1, const Point3D& _p2, const Point3D& _p3)
    {
        auto v12 = _p2 - _p1;
        auto v13 = _p3 - _p1;
        normal = crossProduct3D(v12, v13);
        d = dotProduct(normal, _p3);
    }
    
};

} // end namespace geo

#endif /* plane_h */
