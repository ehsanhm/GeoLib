//
//  test.h
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-17.
//

#ifndef test_h
#define test_h

#include <iostream>
#include "geoUtils.h"

void Test_Vector()
{
    geo::Vector a(1.0f);
    geo::Vector b(100.0f, 200.0f);
    geo::Vector c(1.0f, 2.0f, 3.0f);
    geo::Vector<float, 5> d({10.0f, 20.0f, 30.0f, 40.0f, 50.0f});
    geo::Vector<int, 2> e;
    
    geo::Vector f = a + c;
    
    a = a + c;
    a.printElements();
    b.printElements();
    e.printElements();

    std::cout << (a < b ? "Yes" : "No") << std::endl;
    std::cout << b[130] << std::endl;
    
    b.assign(1, 150.0f);
    b.printElements();
    
    geo::Vector g(0.7071, 0.7071);
    geo::Vector h(0.7071, 0.7071, 0.0);


    float dot = dotProduct(g, h);
    std::cout << "g dot h = " << dot << std::endl;

    geo::Vector<float, 2> g2(2.0, 0.0);
    geo::Vector<float, 2> h2(0.0, 3.0);
    float cross = crossProduct2D(g2, h2);
    std::cout << "g2 cross h2 = " << cross << std::endl;

    geo::Vector3f g3(1.0, 0.0, 0.0);
    geo::Vector3f h3(0.0, 1.0, 0.0);
    geo::Vector3f g3_cross_h3 = crossProduct3D(g3, h3);
    g3_cross_h3.printElements();

    geo::Vector3f i3(4.0, 2.0, 4.0);
    std::cout << "i3 magnitude = " << i3.magnitude() << std::endl;

    geo::Vector3f i3_norm = i3.normal();
    i3_norm.printElements();

    geo::Vector3f j3(4.0, 2.0, 4.0);
    j3.normalize();
    j3.printElements();
}


void Test_TriangleArea()
{
    geo::Point2D a(0.0f, 0.0f);
    geo::Point2D b(0.0f, 1.0f);
    geo::Point2D c(0.0f, 10.0f);
    
    auto area = geo::getTriangleArea2D(a, b, c);
    std::cout << "area of abc triangle is: " << area << std::endl;

    geo::printOrientation2D(a, b, c);
    
    std::cout << "Relative position of c compared to ab is Left or Beyond? " << geo::leftOrBeyond(a, b, c) << std::endl;

}


void Test_Line()
{
    geo::Vector3f point(0.0f, 0.0f, 0.0f);
    geo::Vector3f dir(1.0f, 0.0f, 0.0f);
    geo::Line a(point, dir);
}


void Test_Plane()
{
    geo::Vector3f normal(0.0f, 1.0f, 0.0f);
    float constant = 4.0f;
    geo::Plane a(normal, constant);
}

#endif /* Test_h */
