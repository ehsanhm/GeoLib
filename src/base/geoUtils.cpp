//
//  geoUtils.cpp
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-20.
//

#ifndef geoUtils_cpp
#define geoUtils_cpp

#include "geoUtils.h"

namespace geo
{


float getTriangleArea2D(const Point2D& a, const Point2D& b, const Point2D& c)
{
    auto ab = b - a;
    auto ac = c - a;
    auto area = crossProduct2D(ab, ac) / 2;
    return area;
}


RELATIVE_POSITION getOrientation2D(const Point2D& a, const Point2D& b, const Point2D& c)
{
    // get area
    auto area = getTriangleArea2D(a, b, c);
    
    // if area is smaller than epsilon, assume it's zero
    if (area > 0 && area < EPSILON)
    {
        area = 0;
    }
    if (area < 0 && area > EPSILON)
    {
        area = 0;
    }
    
    // c is on the left of ab
    if (area > 0)
    {
        return RELATIVE_POSITION::LEFT;
    }
    
    // c is on the right of ab
    if (area < 0)
    {
        return RELATIVE_POSITION::RIGHT;
    }
    
    // c is on a
    if (c == a)
    {
        return RELATIVE_POSITION::ORIGIN;
    }
    
    // c is on b
    if (c == b)
    {
        return RELATIVE_POSITION::DESTINATION;
    }
    
    // c is behind ab
    auto ab = b - a;
    auto ac = c - a;
    if (ab[X] * ac[X] < 0 || ab[Y] * ac[Y] < 0)
    {
        return RELATIVE_POSITION::BEHIND;
    }
    
    // c is beyond ab
    if (ac.magnitude() > ab.magnitude())
    {
        return RELATIVE_POSITION::BEYOND;
    }
    
    // c in between a and b
    return RELATIVE_POSITION::BETWEEN;
    
}


void printOrientation2D(const Point2D& a, const Point2D& b, const Point2D& c)
{
    auto relative_position = getOrientation2D(a, b, c);
    std::string relative_position_str = "";
    switch (relative_position) {
        case RELATIVE_POSITION::LEFT:
            relative_position_str = "LEFT";
            break;
        case RELATIVE_POSITION::RIGHT:
            relative_position_str = "RIGHT";
            break;
        case RELATIVE_POSITION::BEHIND:
            relative_position_str = "BEHIND";
            break;
        case RELATIVE_POSITION::BEYOND:
            relative_position_str = "BEYOND";
            break;
        case RELATIVE_POSITION::BETWEEN:
            relative_position_str = "BETWEEN";
            break;
        case RELATIVE_POSITION::ORIGIN:
            relative_position_str = "ORIGIN";
            break;
        case RELATIVE_POSITION::DESTINATION:
            relative_position_str = "DESTINATION";
            break;
        default:
            relative_position_str = "ERROR";
            break;
    }
    std::cout << "Relative position of c compared to ab is: " << relative_position_str << std::endl;
}

bool left(const Point2D& a, const Point2D& b, const Point2D& c)
{
    return getOrientation2D(a, b, c) == RELATIVE_POSITION::LEFT;
}

bool right(const Point2D& a, const Point2D& b, const Point2D& c)
{
    return getOrientation2D(a, b, c) == RELATIVE_POSITION::RIGHT;
}

bool leftOrBeyond(const Point2D& a, const Point2D& b, const Point2D& c)
{
    auto relative_position = getOrientation2D(a, b, c);
    return (relative_position == RELATIVE_POSITION::LEFT || relative_position == RELATIVE_POSITION::BEYOND);
}

bool leftOrBetween(const Point2D& a, const Point2D& b, const Point2D& c)
{
    auto relative_position = getOrientation2D(a, b, c);
    return (relative_position == RELATIVE_POSITION::LEFT || relative_position == RELATIVE_POSITION::BETWEEN);
}


} // end namespace geo

#endif /* geoUtils_cpp */
