//
//  core.h
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-15.
//

#ifndef core_h
#define core_h

#include <iostream>
#include <array>
#include <float.h>
#include <math.h>

#define EPSILON 0.0000001

enum RELATIVE_POSITION
{
    LEFT,
    RIGHT,
    BEHIND,
    BEYOND,
    BETWEEN,
    ORIGIN,
    DESTINATION
};

static inline bool isEqual(double x, double y)
{
    return abs(x - y) < EPSILON;
}

#endif /* core_h */
