//
//  vector.cpp
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-17.
//

#include "vector.h"


// crossProduct2D(v1, v2)
float geo::crossProduct2D(const Vector2f v1, const Vector2f v2)
{
    return v1[X] * v2[Y] - v1[Y] * v2[X];
}

// crossProduct3D(v1, v2)
geo::Vector3f geo::crossProduct3D(const Vector3f v1, const Vector3f v2)
{
    float x = v1[Y] * v2[Z] - v1[Z] * v2[Y];
    float y = v1[X] * v2[Z] - v1[Z] * v2[X];
    float z = v1[X] * v2[Y] - v1[Y] * v2[X];
    return Vector3f(x, -y, z);
}
