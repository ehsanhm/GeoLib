//
//  vector.h
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-09.
//

#ifndef vector_h
#define vector_h

#include "core.h"

#define DIM2 2
#define DIM3 3

#define X 0
#define Y 1
#define Z 2


namespace geo
{

template<class T, size_t dimensions=DIM3>
class Vector
{    
private:
    // check inputs
    static_assert(dimensions >= 2, "dimensions must be 2 or larger!");
    static_assert(std::is_arithmetic_v<T>, "type must be of number type!");
    
    // variables
    std::array<T, dimensions> coords;
    
    // friends
    template<class TT, size_t dim>
    friend float dotProduct(const Vector<TT, dim>& v1, const Vector<TT, dim>& v2);
        
public:
    ~Vector(){}
    
    // default
    Vector(){
//        std::cout << "Vector default constructor called!" << std::endl;
    }
    
    // unknown elements
    Vector(std::array<T, dimensions> _coords)
    {
        coords = _coords;
//        std::cout << "Vector array constructor called!" << std::endl;
    }
    
    // 3 elements using one argument
    Vector(T _x)
    {
        coords = {_x, _x, _x};
//        std::cout << "Vector3 using one argument constructor called!" << std::endl;
    }
    
    // 2 elements
    Vector(T _x, T _y)
    {
        coords = {_x, _y};
//        std::cout << "Vector2 constructor called!" << std::endl;
    }
    
    // 3 elements
    Vector(T _x, T _y, T _z)
    {
        coords = {_x, _y, _z};
//        std::cout << "Vector3 constructor called!" << std::endl;
    }
    
    // print elements
    void printElements()
    {
        std::cout << "[";
        for (auto i: coords)
        {
            std::cout << i << ", ";
        }
        std::cout << "]" << std::endl;
    }
    
    // set
    void setElements(std::array<T, dimensions> _coords)
    {
        coords = _coords;
    }
    
    // ==
    bool operator==(const Vector<T, dimensions>& other) const;
    
    // !=
    bool operator!=(const Vector<T, dimensions>& other) const;
    
    // +
    Vector<T, dimensions> operator+(const Vector<T, dimensions>& other) const;
    
    // -
    Vector<T, dimensions> operator-(const Vector<T, dimensions>& other) const;
    
    // +=
    void operator+=(const Vector<T, dimensions>& other);
    
    // -=
    void operator-=(const Vector<T, dimensions>& other);
    
    // =
    void operator=(const Vector<T, dimensions>& other)
    {
        coords = other.coords;
    }
    
    // <
    bool operator<(const Vector<T, dimensions>& other) const;

    // >
    bool operator>(const Vector<T, dimensions>& other) const;
    
    // []
    T operator[](int index) const;
    
    // assignment
    void assign(size_t dim, T value);
    
    // magnitude
    float magnitude() const;
    
    // normal
    Vector<T, dimensions> normal() const;
    
    // normalize
    void normalize();
    
}; // end class definition Vector

// ==
template<class T, size_t dimensions>
inline bool Vector<T, dimensions>::operator==(const Vector<T, dimensions>& other) const
{
    for (size_t i = 0; i < dimensions; i++)
    {
        if (!isEqual(coords[i], other.coords[i]))
        {
            return false;
        }
    }
    return true;
}

// !=
template<class T, size_t dimensions>
inline bool Vector<T, dimensions>::operator!=(const Vector<T, dimensions>& other) const
{
    return !(*this == other);
}

// +
template<class T, size_t dimensions>
inline Vector<T, dimensions> Vector<T, dimensions>::operator+(const Vector<T, dimensions>& other) const
{
    Vector<T, dimensions> tempVec;
    for (size_t i = 0; i < dimensions; i++)
    {
        tempVec.coords[i] = coords[i] + other.coords[i];
    }
    return tempVec;
}

// -
template<class T, size_t dimensions>
inline Vector<T, dimensions> Vector<T, dimensions>::operator-(const Vector<T, dimensions>& other) const
{
    Vector<T, dimensions> tempVec;
    for (size_t i = 0; i < dimensions; i++)
    {
        tempVec.coords[i] = coords[i] - other.coords[i];
    }
    return tempVec;
}

// +=
template<class T, size_t dimensions>
inline void Vector<T, dimensions>::operator+=(const Vector<T, dimensions>& other)
{
    for (size_t i = 0; i < dimensions; i++)
    {
        coords[i] += other.coords[i];
    }
}

// -=
template<class T, size_t dimensions>
inline void Vector<T, dimensions>::operator-=(const Vector<T, dimensions>& other)
{
    for (size_t i = 0; i < dimensions; i++)
    {
        coords[i] -= other.coords[i];
    }
}

// <
template<class T, size_t dimensions>
inline bool Vector<T, dimensions>::operator<(const Vector<T, dimensions>& other) const
{
    for (size_t i = 0; i < dimensions; i++)
    {
        if (coords[i] < other.coords[i])
        {
            return true;
        }
        else if (coords[i] > other.coords[i])
        {
            return false;
        }
    }
    return false;
}

// >
template<class T, size_t dimensions>
inline bool Vector<T, dimensions>::operator>(const Vector<T, dimensions>& other) const
{
    for (size_t i = 0; i < dimensions; i++)
    {
        if (coords[i] > other.coords[i])
        {
            return true;
        }
        else if (coords[i] < other.coords[i])
        {
            return false;
        }
    }
    return false;
}

// []
template<class T, size_t dimensions>
inline T Vector<T, dimensions>::operator[](int index) const
{
    if (index >= coords.size())
    {
        std::cout << "index out of range" << std::endl;
        return T();
    }
    return coords[index];
}

// assignment
template<class T, size_t dimensions>
inline void Vector<T, dimensions>::assign(size_t dim, T value)
{
    if (dim >= coords.size())
    {
        std::cout << "Index out of range" << std::endl;
    }
    coords[dim] = value;
}

// dotProduct(v1, v2)
template<class TT, size_t dim>
float dotProduct(const Vector<TT, dim>& v1, const Vector<TT, dim>& v2)
{
    if (v1.coords.size() != v2.coords.size())
    {
        std::cout << "Vector sizes don't match. returning FLT_MIN!" << std::endl;
        return FLT_MIN;
    }
    
    float result = 0.0;
    for (size_t i = 0; i < v1.coords.size(); i++)
    {
        result += v1.coords[i] * v2.coords[i];
    }
    return result;
}

// magnitude
template<class T, size_t dimensions>
inline float Vector<T, dimensions>::magnitude() const
{
    float mag = 0.0f;
    
    for (size_t i = 0; i < dimensions; i++)
    {
        mag += pow(coords[i], 2);
    }
    
    return sqrt(mag);
}

// normal
template<class T, size_t dimensions>
inline Vector<T, dimensions> Vector<T, dimensions>::normal() const
{
    float mag = magnitude();
    Vector<T, dimensions> result;
    for (size_t i = 0; i < dimensions; i++)
    {
        result.coords[i] = coords[i] / mag;
    }
    return result;
}

// normalize
template<class T, size_t dimensions>
inline void Vector<T, dimensions>::normalize()
{
    float mag = magnitude();
    for (size_t i = 0; i < dimensions; i++)
    {
        coords[i] /= mag;
    }    
}

// typedef
typedef Vector<float, DIM2> Vector2f;
typedef Vector<float, DIM3> Vector3f;

float crossProduct2D(const Vector2f v1, const Vector2f v2);
Vector3f crossProduct3D(const Vector3f v1, const Vector3f v2);

} // end namespace geo

#endif /* vector_h */
