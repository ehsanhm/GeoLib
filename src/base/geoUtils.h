//
//  geoUtils.h
//  GeoLib
//
//  Created by Ehsan Hassani Moghaddam on 2022-06-19.
//

#ifndef geoUtils_h
#define geoUtils_h

#include "point.h"
#include "line.h"
#include "plane.h"

namespace geo
{

float getTriangleArea2D(const Point2D& a, const Point2D& b, const Point2D& c);

RELATIVE_POSITION getOrientation2D(const Point2D& a, const Point2D& b, const Point2D& c);

void printOrientation2D(const Point2D& a, const Point2D& b, const Point2D& c);

bool left(const Point2D& a, const Point2D& b, const Point2D& c);

bool right(const Point2D& a, const Point2D& b, const Point2D& c);

bool leftOrBeyond(const Point2D& a, const Point2D& b, const Point2D& c);

bool leftOrBetween(const Point2D& a, const Point2D& b, const Point2D& c);

} // end namespace geo

#endif /* geoUtils_h */
